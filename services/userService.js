const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getUsersList() {
        const usersList = UserRepository.getAll();
        if(!usersList.length) {
            return null;
        }
        return usersList;
    }

    createUser(data) {
        const user = UserRepository.create(data);
        if(!user) {
            return null;
        }
        return user;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    updateUser(id, data) {
        const item = UserRepository.update(id, data);
        if(!item) {
            return null;
        }
        return item;
    }

    deleteUser(id) {
        const item = UserRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();