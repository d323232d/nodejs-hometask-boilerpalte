const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getFightersList() {
        const fightersList = FighterRepository.getAll();
        if(!fightersList.length) {
            return null;
        }
        return fightersList;
    }

    createFighter(data) {
        const fighter = FighterRepository.create(data);
        if(!fighter) {
            return null;
        }
        return fighter;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    updateFighter(id, data) {
        const item = FighterRepository.update(id, data);
        if(!item) {
            return null;
        }
        return item;
    }

    deleteFighter(id) {
        const item = FighterRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();