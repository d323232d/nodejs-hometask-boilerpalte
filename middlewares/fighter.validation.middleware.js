const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    if(isEmpty(req.body)) {
        return res.status(400).send({
          error: true,
          message: 'No data for creating'
        })
    };

    for(let key in fighter) {
        if(key === 'id' || key === 'health') continue;

        if(!req.body.hasOwnProperty(key)) {
            return res.status(400).send({
                error: true,
                message: `Fighter must have field ${key}`
            })
        }
    };
    for(let key in req.body) {
        if(!fighter.hasOwnProperty(key)) {
            return res.status(400).send({
                error: true,
                message: `Fighter shouldn't have field ${key}`
            })
        }
    };
    const {name, health, power, defense} = req.body;
    let fightersList = FighterService.getFightersList();
    let existName = false;
    
    if(fightersList) {
        existName = fightersList.length 
        ? fightersList.some(item => item.name.toLowerCase() === name.toLowerCase()) 
        : false;
    }    
    
    if(existName) {
        return res.status(400).send({
            error: true,
            message: `Fighter with name ${name} alredy exist in DB`
        })
    }
    if(!name.trim().length) {
        return res.status(400).send({
            error: true,
            message: 'Input fighter\'s name'
        });
    }
    
    if(isNaN(defense) || defense < 1 || defense > 10) {
        return res.status(400).send({
            error: true,
            message: 'Defense must be a number from 1 to 10'
        });
    }
    if(isNaN(power) || power < 1 || power > 100) {
        return res.status(400).send({
            error: true,
            message: 'Power must be a number from 1 to 100'
        });
    } 
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    
    if(!FighterService.search(req.params)) return res.status(404).send({        
        error: true,
        message: "Valid: Fighter not found"
    });

    if(isEmpty(req.body)) {
        return res.status(400).send({
            error: true,
            message: 'Nothing to change'
        })
    }
    
    for(let key in req.body) {
        if(key === 'id') continue;
        if(!fighter.hasOwnProperty(key)) {
            return res.status(400).send({
                error: true,
                message: `Fighter doesn't have field ${key}`
            })
        }
    }

    if(req.body.hasOwnProperty('name')) {
        let fightersList = FighterService.getFightersList();
        
        if(!fightersList) {
            return res.status(404).send({
                error: true,
                message: "DB is empty yet. Nothing to update"
            });
        }
        let existName = fightersList.some(item => item.name.toLowerCase() === req.body.name.toLowerCase());
        if(existName) {
            return res.status(400).send({
                error: true,
                message: `Fighter with name ${req.body.name} alredy exist in DB`
            })
        }
        if(!req.body.name.trim().length) {
            return res.status(400).send({
                error: true,
                message: 'Input fighter\'s name'
            });
        }
    }

    if(req.body.hasOwnProperty('health')) {
        if(isNaN(req.body.health) || req.body.health < 80 || req.body.health > 120) {
            return res.status(400).send({
                error: true,
                message: 'Health must be a number from 80 to 120'
            });
        }
        
    }
    if(req.body.hasOwnProperty('power')) {
        if(isNaN(req.body.power) || req.body.power < 1 || req.body.power > 100) {
            return res.status(400).send({
                error: true,
                message: 'Power must be a number from 1 to 100'
            });
        }
    }
    if(req.body.hasOwnProperty('defense')) {
        if(isNaN(req.body.defense) || req.body.defense < 1 || req.body.defense > 10) {
            return res.status(400).send({
                error: true,
                message: 'Defense must be a number from 1 to 10'
            });
        }
    }

    

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

function isEmpty(obj) {
    for (let key in obj) {
      return false;
    }
    return true;
  }