const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   
    if (
        req &&
        req.headers &&
        req.headers.authorization &&
        req.headers.authorization === 'user') { 
        
        next();
    } else {
        res.send({
            error: true,
            message: 'No authorized'
          })
    }

    next();
}

exports.responseMiddleware = responseMiddleware;