const { user } = require('../models/user');
const UserService = require('../services/userService');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    if(isEmpty(req.body)) {
        return res.status(400).send({
            error: true,
            message: 'No data for create'
        })
    }
    
    for(let key in user) {
        if(key === 'id') continue;

        if(!req.body.hasOwnProperty(key)) {
            return res.status(400).send({
                error: true,
                message: `User must have field ${key}`
            })
        }
    }

    for(let key in req.body) {
        if(!user.hasOwnProperty(key)) {
            return res.status(400).send({
                error: true,
                message: `User shoudn't have field ${key}`
            })
        }
    }

    const {firstName, lastName, email, phoneNumber, password} = req.body;
    
    const regEmail = /^[-._a-z0-9]+@gmail\.com/igm;
    const regPhone = /^\+380[0-9]{9}/gmi;
    const regName = /^[a-zA-Z0-9-_\.]{1,20}$/igm;
    const regPass = /^[a-zA-Z0-9-_@]{3,20}$/igm;

    if(!regName.test(firstName.trim())) {
       return res.status(400).send({
            error: true,
            message: 'Input your firstName - max 20 characters'
        })
    } 

    if(!lastName.trim().length) {
       return res.status(400).send({
            error: true,
            message: 'Input your lastName - max 20 characters'
        })
    } 

    if(!regEmail.test(email)) {
        return res.status(400).send({
            error: true,
            message: 'Input email in format XXXX@gmail.com'
        });
    } 
    if(!regPhone.test(phoneNumber)) {
        return res.status(400).send({
            error: true,
            message: 'Input phone number in format +380XXXXXXXXXX'
        })
    }

    

    let usersList = UserService.getUsersList();
    let existEmail = false;
    let existPhone = false;
    if(usersList) {
        existEmail = usersList.length 
            ? usersList.some(item => item.email === email) 
            : false;
        existPhone = usersList.length 
            ? usersList.some(item => item.phoneNumber === phoneNumber)
            : false;
    }

    if(existEmail) {
        return res.status(400).send({
            error: true,
            message: `User with email ${email} alredy exist in DB`
        })
    }

    if(existPhone) {
        return res.status(400).send({
            error: true,
            message: `User with phone number ${phoneNumber} alredy exist in DB`
        })
    }

      

    if(!regPass.test(password)) {
        return res.status(400).send({
            error: true,
            message: 'Input your password min=3 chars, max=20 chars'
        })
    }

    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    
    if(!UserService.search(req.params)) return res.status(404).send({
        error: true,
        message: "User not found"
    });

    if(isEmpty(req.body)) {
        return res.status(400).send({
            error: true,
            message: 'Nothing to change'
        })
    }

    for(let key in req.body) {
        if(!user.hasOwnProperty(key)) {
            return res.status(400).send({
                error: true,
                message: `User doesn't have field ${key}`
            })
        }
    }
    
    if(req.body.hasOwnProperty('firstName') && !req.body.firstName.trim().length) {
        
        return res.status(400).send({
            error: true,
            message: `User firstName can't be empty string`
        })
    }
   
    if(req.body.hasOwnProperty('lastName') && !req.body.lastName.trim().length) {
        return res.status(400).send({
            error: true,
            message: `User lastName can't be empty string`
        })
    }

    if(req.body.hasOwnProperty('email')) {
        const regEmail = /^[-._a-z0-9]+@gmail\.com/igm;        
        if(!regEmail.test(req.body.email)) {
            return res.status(400).send({
                error: true,
                message: 'Input email in format XXXX@gmail.com'
            });
        }
        
        let users = UserService.getUsersList();
        if(!users) {
            return res.status(404).send({
                error: true,
                message: "DB is empty yet. Nothing to update"
            });
        }
        let existEmail = users.some(item => item.email === req.body.email);
       
        if(existEmail) {
            return res.status(400).send({
                error: true,
                message: `User with email ${req.body.email} alredy exist in DB`
            })
        }
    };

    if(req.body.hasOwnProperty('phoneNumber')) {
        const regPhone = /^\+380[0-9]{9}/gmi;
        if(!regPhone.test(req.body.phoneNumber)) {
            return res.status(400).send({
                error: true,
                message: 'Input phone number in format +380XXXXXXXXXX'
            })
        }
        let users = UserService.getUsersList();
        if(!users) {
            return res.status(404).send({
                error: true,
                message: "DB is empty yet. Nothing to update"
            });
        }
        
        let existPhone = users.some(item => item.phoneNumber === req.body.phoneNumber);
        if(existPhone) {
            return res.status(400).send({
                error: true,
                message: `User with phone number ${req.body.phoneNumber} alredy exist in DB`
            })
        }
    };

    if(req.body.hasOwnProperty('password')) {
        const regPass = /^[a-zA-Z0-9-_@]{3,20}$/igm;
        if(!regPass.test(req.body.password)) {
            return res.status(400).send({
                error: true,
                message: 'Input your password min=3 chars, max=20 chars'
            })
        }
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }
  return true;
}