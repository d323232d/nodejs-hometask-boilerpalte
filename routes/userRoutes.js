const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user
router.get('/', function(req, res, next) {
  let usersList = UserService.getUsersList();
  if(!usersList) {
    return res.status(404).send({
      error: true,
      message: "DB is empty yet"
    });
  }
  // let list = [...usersList];
  // list.forEach(element => {
  //   delete element.id
  // });
  res.status(200).send(usersList);
});

router.get('/:id', function(req, res, next) {
  const user = UserService.search(req.params);
  
  if(!user) {
    return res.status(404).send({
      error: true,
      message: "User not found"
    });
  }
  // delete user.id;
  res.status(200).send(user);
});

router.post('/', createUserValid, function(req, res, next) {
  
  const {firstName, lastName, email, phoneNumber, password} = req.body;
  
  const userData = {
    firstName,
    lastName, 
    email, 
    phoneNumber, 
    password
  };
     
  const newUser = UserService.createUser(userData);
  // delete newUser.id;
  res.status(200).send(userData);
});

router.put('/:id', updateUserValid, function(req, res, next) {
  const user = UserService.search(req.params);
  if(!user) {
    return res.status(404).send({
      error: true,
      message: "User not found"
    });
  };

  let change = {
    ...user,
    ...req.body
  };
  
  UserService.updateUser(req.params.id, change);
  delete change.id;
  res.status(200).send(change);
});

router.delete('/:id', function(req, res, next) {
  const userForDelete = UserService.search(req.params); 
  if(!userForDelete) {
    return res.status(404).send({
      error: true,
      message: "User not found"
    });
  }; 
  UserService.deleteUser(req.params.id);
  delete userForDelete.id;
  res.status(200).send(userForDelete);
} )

module.exports = router;

function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }
  return true;
}