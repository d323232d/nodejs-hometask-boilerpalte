const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', function(req, res, next) {
  let fightersList = FighterService.getFightersList();
  if(!fightersList) {
    return res.status(404).send({
      error: true,
      message: "DB is empty yet"
    });
  }
  // fightersList.forEach(element => {
  //   delete element.id
  // });
  res.send(fightersList);
});
router.get('/:id', function(req, res, next) {
  const fighter = FighterService.search(req.params);
  if(!fighter) {
    return res.status(404).send({
      error: true,
      message: "Fighter not found"
    });
  }
  
  // delete fighter.id;
  res.status(200).send(fighter);
});

router.post('/', createFighterValid, function(req, res, next) {
  const {name, health, power, defense} = req.body;
  
  const fighterData = {
    name, 
    health: 100, 
    power, 
    defense
  };
  
  FighterService.createFighter(fighterData);
  // delete fighterData.id;  
  res.status(200).send(fighterData);
  
});

router.put('/:id', updateFighterValid, function(req, res, next) {
  const fighter = FighterService.search(req.params);
  if(!fighter) {
    return res.status(404).send({
      error: true,
      message: "Fighter not found"
    });
  };
  let change = {
    ...fighter,
    ...req.body
  };
  
  FighterService.updateFighter(req.params.id, change);
  // delete change.id;
  res.status(200).send(change);
});

router.delete('/:id', function(req, res, next) {
  const fighterForDelete = FighterService.search(req.params); 
  if(!fighterForDelete) {
    return res.status(404).send({
      error: true,
      message: "Fighter not found"
    });
  }; 
  FighterService.deleteFighter(req.params.id);
  delete fighterForDelete.id;
  res.status(200).send(fighterForDelete);
  
} )



module.exports = router;

function isEmpty(obj) {
  for (let key in obj) {
    return false;
  }
  return true;
}