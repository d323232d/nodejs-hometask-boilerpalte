const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights
router.get('/', function(req, res, next) {
  console.log('req.body: ', req.body);
  
    
    // let winner = FightService.fight(req.body);
    
    res.send("Let's go")
  
})

router.post('/', function(req, res, next){
  console.log("Let's fight");
  const [first, second] = req.body
  FightService.fight(first, second);
  res.send("Let's fight")
})

module.exports = router;