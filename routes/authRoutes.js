const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        
        let data = AuthService.login(req.body);        
        if(data) {
            res.send(req.body);
        } 
        next();
    } catch (err) {
        res.status(401).send({
            error: true,
            message: 'User not found'
          });
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;